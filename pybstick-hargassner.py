from machine import Pin
from machine import SoftI2C
import time
from apa106 import APA106
import network
import socket
from machine_i2c_lcd import I2cLcd
import os
import gc

BOILER_IP="192.168.0.3"
BOILER_PORT=23
BOILER_FIRMWARE="14g"

VALUE_STATE=1

STATE_MSG = {
"1":"Arret",
"2":"Allumage",
"3":"Demarrage\nchaudiere",
"4":"Controle\nallumage",
"5":"Allumage",
"6":"Demarrage\ncombustion",
"7":"Combustion",
"8":"Veille",
"9":"Decendrage\ndans 7 mn",
"10":"Decendrage",
"11":"Refroidissement",
"12":"Nettoyage",
"17":"Assistant de combustion"
}

DEBUG = True
def log(s):
  if DEBUG:
    print(s)
    
# Connect screen
i2c = SoftI2C( sda=Pin(3), scl=Pin(4) )

# scanne les esclaves disponibles et retourne une list d'adresses 7-bits
i2c.scan()
#RGB LED
rgb_led = Pin(8, Pin.OUT)
ap = APA106(rgb_led, 1)
ap[0] = (0, 0, 64) # set the led to blue
ap.write()

#red led
red_led = Pin(6, Pin.OUT)
red_led.off()

lcd = I2cLcd(i2c, 0x27, 2, 16)
lcd.putstr("Initializing")

# Activate station mode
sta_if = network.WLAN(network.STA_IF)
log(sta_if.active())

# Connect to network
if not sta_if.isconnected():
  sta_if.active(True)
  sta_if.connect('baurens', '123456789')
  while not sta_if.isconnected():
    lcd.clear()
    lcd.putstr('connecting to network...')
    time.sleep(1)
    pass
  log('network config:'+ str(sta_if.ifconfig()))

log("Connected to AP")

# Connect to boiler, blink red until connexion is OK
def connect():
  while True:
    try:
      ap[0] = (0, 0, 64) # set the led to Blue
      ap.write()
      time.sleep(1)
      s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
      s.connect((BOILER_IP, BOILER_PORT))
      log("Socket connected")
      ap[0] = (0, 0, 0) # turn off
      ap.write()
      time.sleep(1)
      return s
      break
    except Exception as e:
      print("Telnet connexion error", e)

last_state = 0
bufferString = ""
bufferComplete = False
fillBuffer = False
remainingBuffer = ""

s = connect()

# receive data from boiler
while True:
  try:
    error_count = 0
    if remainingBuffer != "":
        log("Use remaining buffer " + remainingBuffer)
        buffer = remainingBuffer
        remainingBuffer = ""
    else :
        buffer = s.recv(600).decode("utf-8") # waiting a packet (waiting as long as s.recv is empty)
    log("got message : " + buffer)
    
    fillBuffer = True
    for i in range(0,len(buffer)):
        if buffer[i:i+2] == 'pm':
            # start
            fillBuffer = False
            bufferString = buffer[i:len(buffer)]
            log("begin")
            continue
        elif buffer[i] == "\n":
            #end
            bufferString = bufferString + buffer[0:i]
            bufferComplete = True
            fillBuffer = False
            remainingBuffer = buffer[i+1:len(buffer)]
            log("end")
            break;
        
    if fillBuffer:
        bufferString = bufferString + buffer
    
    if bufferComplete :
      bufferComplete = False
      # got a full message, decode it
      error_count = 0
      state = bufferString[0:10].split(' ')[VALUE_STATE]
      log("full message received")
      log("state : " + state)
      state_msg = STATE_MSG.get(state, "Unknown")
      if (last_state != state):
        lcd.clear()
        lcd.putstr(state_msg)
        last_state = state
      if state == "1":
        red_led.on()
        # state KO
        ap[0] = (0, 64, 0) # set the led to red
        ap.write()
      else :
        #state OK
        red_led.off()
        ap[0] = (64, 0, 0) # set the led to green
        ap.write()
          
    else:
      error_count = error_count + 1
      if error_count >= 10:
        red_led.on()
        # should have received a complete buffer by then
        log("buffer ERROR pm")
        ap[0] = (0, 64, 0) # set the led to green
        ap.write()
        time.ms_sleep(300)
        ap[0] = (0, 0, 0) # set the led off
        ap.write()
        time.ms_sleep(300)

    time.sleep(1)
  except Exception as e:
    print("buffer recv ERROR", e)
    raise
    
not_end = 1
