Small project to retrieve status information from my Hargassner boiler NanoPK using a PYBStick26 ESP32C3.

The boiler is connected to my local network and data can be retrieved using telnet. I'm using a I2C connected 1602A LCD screen, a push button to active LCD light only when needed and a red LED to alert when there is a problem.

Inspired from : https://github.com/Jahislove/hargassner-python, which provides a much heavier version saving data to a database.

lcd_api.py and machine_i2c_lcd.py are used to control le 1602A screen.

Released under GNU GPL v3.

